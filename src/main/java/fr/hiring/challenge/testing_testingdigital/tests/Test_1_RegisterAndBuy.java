package fr.hiring.challenge.testing_testingdigital.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

import fr.hiring.challenge.testing_testingdigital.pages.Base;
import fr.hiring.challenge.testing_testingdigital.pages.Registration;
import fr.hiring.challenge.testing_testingdigital.pages.Login;
import fr.hiring.challenge.testing_testingdigital.pages.Dashboard;
import fr.hiring.challenge.testing_testingdigital.pages.Pricing;
import fr.hiring.challenge.testing_testingdigital.pages.Billing;

public class Test_1_RegisterAndBuy extends Base {
	
	// WebDriver Binaries
	private static final String
		FIREFOX_BINARY = "C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe",
		GECKO_DRIVER = "C:\\Program Files (x86)\\Mozilla Firefox\\geckodriver.exe";

	// Data set
	private static final String 
		TVA = "FR50790150890",
		FULL_ADDRESS = "116 Rue de Silly, Boulogne-Billancourt, France",
		STREET_NUMBER = "116",
		ROUTE = "Rue de Silly",
		ZIP_CODE = "92100",
		CITY = "Boulogne-Billancourt",
		STATE = "Île-de-France",
		COUNTRY = "France";
	
	private static final String
		BASE_URL = "https://ua.testingdigital.com/register/";

	private WebDriver driver; 	
	
	private String 
		username,
		password;

	@Before
	public void setUp() {
	    System.out.println("Setting up");
	    
	    System.setProperty("webdriver.gecko.driver", GECKO_DRIVER);
	   
	    FirefoxOptions options = new FirefoxOptions();
		options.setBinary(FIREFOX_BINARY);
	
		driver = new FirefoxDriver(options);
		driver.manage().window().maximize();
	}
	
	@Test
	public void Test() throws InterruptedException {
		System.out.println("(test starts)");
		
	    // Step 1: Go to URL
	    System.out.println("1| Going to " + BASE_URL);
		driver.get(BASE_URL); 
		
		// Step 2: Create a new account with a new "mail address" and "Password"
		System.out.println("2| Registering");
		
		Registration registration = new Registration(driver);
		
		assertEquals(registration.getTopTitle(), "Register for free");
		
		username = 
		registration.enterEmailAddress();
		password =
		registration.enterPassword();
		registration.submit();
		
		// Step 3: Check if we auto redirects to "the authentication page"
		System.out.println("3| Checking autoredirection");
		
		Login login = new Login(driver);
		
		assertTrue(login.isCurrentPage());
		
		// Step 4: Connect you with the new account created
		System.out.println("4| Signing in");
		
		login.enterUsername(username);
		login.enterPassword(password);
		login.submit();
		
		// Step 5: Check that we switch to the page "Dashboard"
		System.out.println("5| Checking current page is the dashboard");
		
		Dashboard dashboard = new Dashboard(driver);
		
		assertEquals(dashboard.getTopTitle(), "Dashboard");
		
		// Step 6: Click on the link " Buy credits " in " Offers area"
		System.out.println("6| Clicking 'Buy credits' in 'Offers area'");
		
		dashboard.buyCredits();
		
		// Step 7: Click on the button " BUY NOW "
		System.out.println("7| Clicking on a button 'BUY NOW''");
		
		Pricing pricing = new Pricing(driver);
		
		pricing.buyNow();
		
		// Step 8: Fill out all the form fields
		System.out.println("8| Filling out all the form fields");
		
		Billing billing = new Billing(driver);
		
		billing.enterTVA(TVA);
		billing.checkCompany();
		billing.enterAddress(FULL_ADDRESS);
		billing.clickOnFirstAddress();
		
		// Step 9: Check if when you fill out the address field [...]
		System.out.println("9| Checking the address");
		
		assertEquals(billing.getStreetNumber(), STREET_NUMBER);
		assertEquals(billing.getRoute(), 		ROUTE);
		assertEquals(billing.getZipCode(), 		ZIP_CODE);
		assertEquals(billing.getCity(),  		CITY);
		assertEquals(billing.getState(), 		STATE);
		assertEquals(billing.getCountry(), 		COUNTRY);

		// Step 10: Logout, and check that we auto redirects to "the authentication page" 
		System.out.println("10| logging out and checking autoredirection");
		
		billing.logout(driver);
		
		assertTrue(login.isCurrentPage());
	}
	
	@After
	public void tearDown() throws Exception {
	    System.out.println("(test ends)");
	    driver.quit();
	}
}
