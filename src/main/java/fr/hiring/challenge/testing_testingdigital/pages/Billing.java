package fr.hiring.challenge.testing_testingdigital.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Billing extends Base {
	
	private WebDriver driver;
	
	private WebElement 
		TVAInput,
		addressInput,
		streenumberInput,
		routeInput,
		zipCodeInput,
		cityInput,
		stateInput,
		countryInput;

	private static final String
		DEFAULT_ADDRESS = "116 Rue de Silly, Boulogne-Billancourt, France";
		
	public Billing(WebDriver driver)  {
		this.driver = driver;
		
		waitUntilElementIsPresent(driver, By.className("col-md-12"), 5);
	}
	
	public void enterTVA(String TVA) {
		
		TVAInput = driver.findElement(By.name("homebundle_user[tva_number]"));
		TVAInput.clear();
		TVAInput.sendKeys(TVA);	
		
		// Important: [TAB] key must be sent in order for the corresponding  
		// company name to appear in the input next to the TVA input (autocompletion)
		TVAInput.sendKeys(Keys.TAB);
		
		System.out.println("\tTVA entered: " + TVA);
	}
	
	public void checkCompany() {		
		WebDriverWait wait = new WebDriverWait(driver, 10);
		
		// Wait until autocompletion is effective
		try {
			wait.until(new ExpectedCondition<Boolean>() {
			    public Boolean apply(WebDriver driver) {
			        return driver.findElement(By.name("homebundle_user[company]"))
			        		.getAttribute("value").length() > 0;
			    }
			});
		}
		catch (TimeoutException e) {
			System.out.println("\t!Error: company name remains empty");
			assert false : "Company name autocomplete failed";
		}
		
		String companyName = driver.findElement(By.name("homebundle_user[company]")).getAttribute("value");
		
		System.out.println("\tCompany name: " + companyName);
	}
	
	public void enterAddress(String address) throws InterruptedException {
		
		if (address == null || address.isEmpty()) {
			address = DEFAULT_ADDRESS;
		}
		
		addressInput = driver.findElement(By.name("homebundle_user[autocomplete_adress]"));
		addressInput.clear();
		addressInput.sendKeys(address);	
	}
	
	public void clickOnFirstAddress() {
		WebDriverWait wait = new WebDriverWait(driver, 5);
		
		// 1. The dynamic dropdown must be visible (autocompletion)
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated
					(By.className("pac-item-query"))
			);
		}
		catch (TimeoutException e) {
			System.out.println("\t!Error: no suggested addresses");
			assert false : "No suggested addresses";
		}
		
		// 2. Suggested addresses appear in splitted SPAN tags
		List<WebElement> matchedAddresses = driver.findElements(By.className("pac-matched"));
		
		// 3. First element is clicked (due to the way the suggested addresses
		//    are structured, it is not possible to use the <WebElement>.click()
		//    method, nor a simple JavaScript action.)
		Actions action = new Actions(driver);
		action.moveToElement(matchedAddresses.get(0)).click().perform();
			
		System.out.println("\tClicked on the first suggested address");
	}
	
	public String getStreetNumber() {
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.id("details_address_hidden"))
		);
		
		streenumberInput = driver.findElement(By.id("street_number"));
		
		String streetNumber = streenumberInput.getAttribute("value").trim();
		
		System.out.println("\tStreet number is: " + streetNumber);
		
		return streetNumber;
	}
	
	public String getRoute() {
		routeInput = driver.findElement(By.id("route"));
		
		String route  = routeInput.getAttribute("value").trim();
		
		System.out.println("\tRoute is: " + route);
		
		return route;
	}
	
	public String getZipCode() {
		zipCodeInput = driver.findElement(By.id("postal_code"));
		
		String zipCode  = zipCodeInput.getAttribute("value").trim();
		
		System.out.println("\tZip Code is: " + zipCode);
		
		return zipCode;
	}
	
	public String getCity() {
		cityInput = driver.findElement(By.id("locality"));
		
		String city  = cityInput.getAttribute("value").trim();
		
		System.out.println("\tCity is: " + city);
		
		return city;
	}

	public String getState() {
		stateInput = driver.findElement(By.id("administrative_area_level_1"));
		
		String state  = stateInput.getAttribute("value").trim();
		
		System.out.println("\tState is: " + state);
		
		return state;
	}
	
	public String getCountry() {
		countryInput = driver.findElement(By.id("country"));
		
		String country  = countryInput.getAttribute("value").trim();
		
		System.out.println("\tCountry is: " + country);
		
		return country;
	}
}
