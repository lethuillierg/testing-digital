package fr.hiring.challenge.testing_testingdigital.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Dashboard extends Base {
	
	private WebDriver driver;
	
	private WebElement topTitle;
	private WebElement buyCreditsLink;
	
	public Dashboard(WebDriver driver)  {
		this.driver = driver;
		
		waitUntilElementIsPresent(driver, By.className("panel-heading"), 5);
	}
	
	public String getTopTitle() {
		topTitle = driver.findElement(By.xpath("//div[@class='container'][1]/h2"));
		return topTitle.getText().trim();
	}
	
	public void buyCredits() {
		try {
			buyCreditsLink = driver.findElement(By.xpath("//div[@class='panel panel-default']/descendant-or-self::a[contains(., 'Buy credits')]"));
		}
		catch (NoSuchElementException e) {
			System.out.println("Error locating buyCreditsLink: check xpath");
		}
		
		buyCreditsLink.click();
	}

}
