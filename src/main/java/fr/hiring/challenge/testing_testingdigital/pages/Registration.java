package fr.hiring.challenge.testing_testingdigital.pages;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Registration extends Base {
	
	private WebDriver driver;
	
	private WebElement topTitle;
	private WebElement emailAddressInput;
	private WebElement passwordInput;
	private WebElement submitButton;
	
	public Registration(WebDriver driver)  {
		this.driver = driver;
		
		waitUntilElementIsPresent(driver, By.className("account-wall"), 5);
	}
	
	/**
	 * Randomly generates an email address
	 */
	
	public String generateEmailAddress() {
		
		Random r = new Random();
		StringBuilder address = new StringBuilder();
		
		// Length of local part of email address 
		// (conservative values for convenience)
		int minLength = 6;
		int maxLength = 30;
		int localLength = r.nextInt(maxLength - minLength + 1) + minLength;
		
		// LOCAL-PART@domain
		char[] local = 
				"abcdefghijklmnopqrstuvwxyz.0123456789".toCharArray();
		
		// local-part@DOMAIN
		// Collection of typical domains...
		List<String> domains = Arrays.asList(
				  "aol.com", 		"facebook.com", 	"gmail.com", 
				  "me.com", 		"hotmail.com", 		"googlemail.com", 
				  "mail.com", 		"msn.com", 			"live.com", 
				  "yahoo.com", 		"hotmail.fr", 		"live.fr", 
				  "laposte.net", 	"yahoo.fr", 		"wanadoo.fr", 
				  "orange.fr", 		"sfr.fr", 			"free.fr"
		);
		
		// ... that is shuffled
		Collections.shuffle(domains);
		
		for(int i = 0; i < localLength; i++) {
			char c = local[r.nextInt(local.length)];
			address.append(c);
	    }
		
		address.append('@').append(domains.get(0));
		
		System.out.println("\tEmail address generated: " + address);
		
		return new String(address);
	}
	
	public String generatePassword() {
		
		Random r = new Random();
		StringBuilder password = new StringBuilder();
		
		int minLength = 10;
		int maxLength = 20;
		int totalLength = r.nextInt(maxLength-minLength) + minLength;
		
		// Chars from decimal range 33..126 in ASCII table
		for(int i = 0; i < totalLength; i++) {
			password.append((char)(r.nextInt(126 - 33 + 1) + 33));
	    }
		
		System.out.println("\tPassword generated: " + password);
		
		return new String(password);
	}
	
	public String getTopTitle() {
		topTitle = driver.findElement(By.xpath("//div[@class='header']/h1"));
		return topTitle.getText().trim();
	}
	
	public String enterEmailAddress() {
		String emailAddress = generateEmailAddress();
		
		emailAddressInput = driver.findElement(By.name("userbundle_user[email]"));
		emailAddressInput.clear();
		emailAddressInput.sendKeys(emailAddress);
		
		System.out.println("\tEmail address entered");
		
		return emailAddress;
	}
	
	public String enterPassword() {
		String password = generatePassword();
		
		passwordInput = driver.findElement(By.name("userbundle_user[password]"));
		passwordInput.clear();
		passwordInput.sendKeys(password);
		
		System.out.println("\tPassword entered");
		
		return password;
	}
	
	public void submit() {
		submitButton = driver.findElement(By.name("userbundle_user[submit]"));
		submitButton.click();
		
		System.out.println("\tSubmitted");
	}
}
