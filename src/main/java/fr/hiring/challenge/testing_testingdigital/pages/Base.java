package fr.hiring.challenge.testing_testingdigital.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Base {
	
	private WebElement logoutLink;
	
	public void waitUntilElementIsPresent(WebDriver driver, By location, int timeOut) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, timeOut);
			wait.until(ExpectedConditions.presenceOfElementLocated
					(location)
			);
		}
		catch(TimeoutException e) {
			// Thread.currentThread().getStackTrace()[2].getClassName(): name of the calling class
			String errorMessage = "Error @ " + Thread.currentThread().getStackTrace()[2].getClassName();
			System.out.println(errorMessage + ": " + e);
			assert false : errorMessage;
		}
	}
	
	public void logout(WebDriver driver) {
		
		try {
			logoutLink = driver.findElement(By.linkText("Logout"));
			logoutLink.click();
		}
		catch (NoSuchElementException e) {
			System.out.println("Error: no logout link");
			assert false : "Logout link missing";
		}
	}
}
