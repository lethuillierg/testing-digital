package fr.hiring.challenge.testing_testingdigital.pages;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Pricing extends Base {
	
	private WebDriver driver;
	
	private List<WebElement> buyNowLinks;
	
	public Pricing(WebDriver driver)  {
		this.driver = driver;
		
		waitUntilElementIsPresent(driver, By.id("bs-pricing-six"), 5);
	}
	
	public void buyNow() {
		buyNowLinks = driver.findElements(By.linkText("Buy Now"));
		
		if (!buyNowLinks.isEmpty()) {
			Random r = new Random();
			int chosenLink = r.nextInt(buyNowLinks.size() - 1);
			
			buyNowLinks.get(chosenLink).click();
			
			System.out.println("\tClicked on button #" + (chosenLink + 1));
		}
		else {
			System.out.println("Error: no 'Buy Now' link found");
			assert false : "'Buy Now' links missing";
		}
	}

}
