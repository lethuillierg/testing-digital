package fr.hiring.challenge.testing_testingdigital.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Login extends Base {

	private WebDriver driver;
	
	private WebElement usernameInput;
	private WebElement passwordInput;
	private WebElement submitButton;
	
	public Login(WebDriver driver)  {
		this.driver = driver;
		
		waitUntilElementIsPresent(driver, By.className("account-wall"), 5);
	}
	
	public boolean isCurrentPage() {
		boolean currentPage = driver.getTitle().contains("Sign in");
			if (!currentPage) {
				System.out.println("\tError: this is not the login page");
			}
		return currentPage;
	}
	
	public void enterUsername(String login) {
		usernameInput = driver.findElement(By.name("userbundle_user[login]"));
		usernameInput.clear();
		usernameInput.sendKeys(login);
		
		System.out.println("\tLogin entered");
	}
	
	public void enterPassword(String password) {
		passwordInput = driver.findElement(By.name("userbundle_user[password]"));
		passwordInput.clear();
		passwordInput.sendKeys(password);
		
		System.out.println("\tPassword entered");
	}
	
	public void submit() {
		submitButton = driver.findElement(By.name("userbundle_user[submit]"));
		submitButton.click();
		
		System.out.println("\tSubmitted");
	}
}
